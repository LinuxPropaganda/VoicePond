# everything here is under the GNU GPL
# https://www.gnu.org/licenses/gpl-3.0.en.html

from flask import Flask, make_response, request, url_for, render_template, redirect, session
import random as r
import json
from glob import glob
import os
import hashlib
from datetime import datetime as dt

import requests

import fleep

with open("settings.json",'r') as f:
    CONFIG = json.load(f)

LOCALHOST = f"http://127.0.0.1:{CONFIG.get('port', 8080)}"

if not os.path.exists("static"):
    os.mkdir("static")

if not os.path.exists("static/uploads"):
    os.mkdir("static/uploads")

def isaudio(path):
    with open(path, "rb") as file:
        info = fleep.get(file.read(128))

    return info.type == ["audio"]


def printd(*args):
    if CONFIG["debug"]:
        print(*args)

def reverse(lst) : 
  return [ele for ele in reversed(lst)]

def DisplayError(error):
    return f'Error: {error}<br/><a href="/">Back to home</a>'


def sync_index(INDEX):

    print("\33[0;37m -= SYNC INDEX =-")
    for address in CONFIG.get("peers", []):
        print("LOADING INDEX FROM", address)
        try:
            their_index = json.loads(requests.get(address+"/get_index").text)
            print("-> \33[1;32mindex downloaded\33[0;37m")
        except:
            print("-> \33[1;31mconnection failed\33[0;37m")
            continue

        for entry, data in their_index.items():
            if entry not in INDEX:
                INDEX[entry] = {"address": address, "title": data["title"], "author": data["author"], "release": data["release"]}


    return INDEX


def load_save(name, alt={}):
    if os.path.exists(name):
        with open(name, 'r') as f:
            return json.load(f)
    else:
        print("creating", name, "database")
        with open(name, 'w') as f:
            f.write(json.dumps(alt))

        return alt


'''
HTML = {}
for i in glob('templates/*'):
    with open(i, 'r') as f:
        HTML[i.split('/')[-1].split('.')[0]] = f.read()
'''

#printd(used_names)


def GenerateName():
    newname = ''
    while newname in used_names+[""]:
        newname = ''.join([r.choice('1234567890') for i in range(8)])

    used_names.append(newname)
    with open('used_names.json', 'w') as f:
        f.write(json.dumps(used_names))


    return newname



def GetImages(match, limit=-1, channels=['']):

    global INDEX
    INDEX = sync_index(INDEX)

    results = []
    i = 0
    for name, data in reverse(INDEX.items()):
        print(name)
        title = data["title"]
        if (match.lower() in title.lower()) or (title.lower() in match.lower()):
            channel = data["author"]
            results.append((name, title, channel))
            i += 1
            if i >= limit and limit>=0:
                break

    if channels!=['']:
        for i in results:
            cringe, cringe, channel = i
            if channel not in channels:
                results.remove(i)
    
    printd([i for i in results])

    return results





app = Flask(__name__)

used_names=load_save("used_names.json", alt=[])
INDEX = load_save("index.json")

INDEX = sync_index(INDEX)




@app.route('/css')
def get_fancy():
    with open('templates/base.css', 'r') as f:
        return f.read()


@app.route('/favicon.ico')
def get_icon():
    with open('bullfrog.ico', 'rb') as f:
        return f.read()





@app.route('/')
def home():
    if session.get('logged_in', 0):
        return render_template('index.html', imgs=GetImages('', 5), user=session['user'])
    else:
        return render_template('index-not-logged.html', imgs=GetImages('', 5))


@app.route('/register', methods=['POST'])
def register():
    user = request.form['username']
    password = request.form['password']
    action = request.form['action']
    
    accounts = load_save("accounts.json")
    
    pwd = hashlib.sha256(password.encode()).hexdigest()
    pwd = str(pwd)
    
    if action == 'register':
        
        if user in accounts:
            return DisplayError('account with this name already exists')
            
        accounts[user] = pwd
        
        with open('accounts.json', 'w') as f:
            f.write(json.dumps(accounts))
        
        session['logged_in'] = True
        session['user'] = user
    
    else:
        
        
        with open('accounts.json', 'r') as f:
            CheckPwd = json.loads(f.read()).get(user, '####cringe####')
        
        if pwd != CheckPwd or CheckPwd == '####cringe####':
            print(pwd, CheckPwd)
            return DisplayError('Failed to login')
    
        session['logged_in'] = True
        session['user'] = user
    
    return redirect('/')


@app.route('/logout')
def logout():
    session['logged_in'] = 0
    session['user'] = ''

    return redirect('/')

@app.route('/search')
def search():
    args = request.args
    match = args['q'].lower()
    channel = args.get('channel', '')

    return render_template('search.html', imgs=GetImages(match), channels=[channel])

@app.route('/get_index')
def get_index():
    return json.dumps(INDEX)


@app.route('/view')
def view():

    args = request.args
    info = INDEX[args["id"]]

    print(info)
    # if here use static/uploads, else redirect to peer sorta

    comments = json.loads(requests.get(f"{info['address']}/static/uploads/{args['id']}/comments.json").text)
    
    title = info["title"]
    author = info['author']
    release = info['release']

    return render_template('watch.html', address=info["address"], title=title, video_id=args['id'], comments=reverse(comments), author=author, release=release)


@app.route('/comment', methods=["POST"])
def comment():
    
    if not session['logged_in']:
        return DisplayError('You must be registered in order to comment')
    
    video_id = request.form['video_id']
    comment = request.form['comment']
    commenter = session['user']
    
    with open(f"static/uploads/{video_id}/comments.json", 'r') as f:
        comments = json.loads(f.read())
    
    comments.append(f'[{commenter}] {comment}')
    
    with open(f"static/uploads/{video_id}/comments.json", 'w') as f:
        f.write(json.dumps(comments))
    
    return redirect(f'/view?id={video_id}')

    

@app.route('/upload')
def upload_video():
    if not session['logged_in']:
        return DisplayError('You must be registered in order to upload')
    
    
    return render_template('upload_video.html')



@app.route('/uploader', methods=["POST"])
def uploader():
    printd('!! recieving upload !!')
    
    # TO CHANGE: title now in info.json, peer in index

    file = request.files['file']
    title = request.form['title']
    
    author = session['user']
    

    printd('|    reading audio file')
    file = file.stream.read() # bytes

    printd('|    generating name')
    newname = GenerateName()

    wdir = 'static/uploads/'+newname
    os.mkdir(wdir)

    printd('|    saving')
    with open(wdir+'/audio', 'wb') as f:
        f.write(file)

    if not isaudio(wdir+'/audio'):
        printd("upload rejected")
        os.remove(wdir+'/audio')
        os.rmdir(wdir)
        return "We accept only audio files, sorry</br><a href='/'>Back to home</a>"
    
    with open(wdir+'/comments.json', 'w') as f:
        f.write(json.dumps([])) 
        
    printd('|    updating index')
    INDEX[newname] = {"title": title, "author": author, "release": dt.now().strftime("%d/%m/%Y"), "address": LOCALHOST}

    with open('index.json', 'w') as f:
        f.write(json.dumps(INDEX))

    return redirect('/')


@app.route('/issue')
def issue():
    return render_template('issue.html')

@app.route('/issuer', methods=["POST"])
def issuer():
    
    text    = request.form['issue']
    respond = request.form.get('email', '')
    date    = dt.now().strftime("%d/%m/%Y")
    
    
    with open('issues.txt', 'a') as f:
        f.write(f"-------------------------------------------\non {date}\n{text}\nRESPOND: {respond}\n")


app.secret_key = os.urandom(12)
app.run(host='0.0.0.0',port=CONFIG.get("port", 8080), debug=CONFIG.get("debug", 0))

